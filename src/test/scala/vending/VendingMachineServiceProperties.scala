package vending

import scala.language.adhocExtensions
import org.scalacheck.*
import org.scalacheck.Test.Parameters
import org.scalacheck.Prop.forAll
import vending.SimpleTypes.*

object VendingMachineServiceProperties extends Properties("VendingMachineServiceProperties"):
  // maximum quantity of any Denomination
  val MAX_QUANTITY = 50
  // vending machine service to test
  val VendingMachineService = VendingMachineGreedyService

  override def overrideParameters(prms: Test.Parameters): Test.Parameters =
    prms.withMinSuccessfulTests(300)
    
  // TODO: Generate a Quantity
  // TODO: Quantity is >= 1 and =< MAX_QUANTITY.
  def genQuantity: Gen[Quantity] =
    ???


  // TODO: Generate a non empty list of Denomination
  // TODO: Pick from Denomination.values a random amount of Denomination
  def genNonEmptyDenominationList: Gen[List[Denomination]] =
    ???


  // TODO: Use both genNonEmptyDenominationList and genQuantity to create a Map[Denomination, Quantity]
  def genNonEmptyChangeMap: Gen[Map[Denomination, Quantity]] =
    ???


  // TODO: Generate a tuple (Map[Denomination, Quantity],Int)
  // TODO: Use the genNonEmptyChangeMap to generate a Map[Denomination, Quantity] m
  // TODO: Additionally, generate an Int in the interval (1..m.size)
  def genMapAndDrop: Gen[(Map[Denomination, Quantity],Int)] =
    ???


  def mapToMoney(m: Map[Denomination, Quantity]): Money =
    m.foldLeft(Money.zero){ case (m, (d,q)) => m + d.value * q}

  def merge(m1: Map[Denomination, Quantity], m2: Map[Denomination, Quantity]): Map[Denomination, Quantity] =
    m1 ++ m2.map( (d,q) => d -> (q + m1.getOrElse(d, Quantity.zero)))  

  property("Exact Change") = forAll(genNonEmptyChangeMap) ( m => {
    val money = mapToMoney(m)
    val opm = VendingMachineService.calculateChange(m, money)
    opm.fold(_ => false, rm => rm == m)
  })

  property("More than Change") = forAll(genNonEmptyChangeMap,genNonEmptyChangeMap) ( (m1,m2) => {
    val money = mapToMoney(m1)
    val opm = VendingMachineService.calculateChange(merge(m1, m2), money)
    opm.fold(_ => false, rm => mapToMoney(rm) == money)
  })  

  property("Less than Change") = forAll(genMapAndDrop) (  (m,d) => {
    val money = mapToMoney(m)
    val opm = VendingMachineService.calculateChange(m.drop(d), money)
    opm.fold(_ => true, _ => false)
  })
